import React from "react";
import moment from "moment";
import styled from "styled-components";
import { connect, } from "react-redux";
import { createSelector, createStructuredSelector, } from 'reselect'
import R from "ramda";

import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import store from "../../redux/store";
import Consts from "../../consts";
import today from "../../lib/today";

const getMetricScores = (store, props) => store.metricScores[props.id];
const getViewingDate = (store, props) => store.currentlyViewingDate;

const getTodaysScore = createSelector(
	[
		getMetricScores,
		getViewingDate,
	],
	(
		scores,
		date,
	) => (scores[date])
);

const makeGetMetricData = () => createStructuredSelector({
	todaysScore: getTodaysScore,
	viewingDate: getViewingDate,
});

export const makeMapStateToProps = () => (
	(state, ownProps) => R.merge(
		ownProps,
		makeGetMetricData()(state, ownProps)
	)
);

const AlignRight = styled.div`
	float: right;
`;

const inputOrOldVal = (input, oldVal) => {
	try{
		return parseInt(input, 10);
	}
	catch(e){
		return oldVal;
	}
};

export default connect( makeMapStateToProps )( (props) => (
	<div>
		<AlignRight>

			<RaisedButton
				label = "-"
				disabled = { (props.viewingDate !== today() ) }
				onClick = { () => store.dispatch({
					type: Consts.Actions.User.Metric.SET,
					id: props.id,
					value: props.todaysScore - 1,
				})}
			/>

			<TextField
				id = { "input" + props.id }
				disabled = { (props.viewingDate !== today() ) }
				value = { props.todaysScore || 0 }
				style = {{
					width: "60px",
				}}

				onChange = { (_, value) => {
					store.dispatch({
						type: Consts.Actions.User.Metric.SET,
						id: props.id,
						value: inputOrOldVal(value, props.todaysScore),
					})
				}}
			/>

			<RaisedButton
				label = "+"
				disabled = { (props.viewingDate !== today() ) }
				onClick = { () => store.dispatch({
					type: Consts.Actions.User.Metric.SET,
					id: props.id,
					value: props.todaysScore + 1,
				})}
			/>

		</AlignRight>
	</div>
));


