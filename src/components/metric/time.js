import React from "react";
import moment from "moment";
import styled from "styled-components";
import { connect, } from "react-redux";
import { createSelector, createStructuredSelector, } from 'reselect'
import R from "ramda";

import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import store from "../../redux/store";
import Consts from "../../consts";
import today from "../../lib/today";

const getMetricScores = (store, props) => store.metricScores[props.id];
const getViewingDate = (store, props) => store.currentlyViewingDate;

const getTodaysScore = createSelector(
	[
		getMetricScores,
		getViewingDate,
	],
	(
		scores,
		date,
	) => (scores[date])
);

const getTodaysScoreFormatted = createSelector(
	[ getTodaysScore, ],
	(score) => {
		let minutes = "" + ( ( score || 0 ) / 60 ) % 60;
		let hours = "" + Math.floor( ( score || 0 ) / 3600 );

		hours = (
			(hours.length === 0)
			? "00"
			: (
				(hours.length ===1)
				? "0" + hours
				: hours
			)
		);

		minutes = (
			(minutes.length === 0)
			? "00"
			: (
				(minutes.length ===1)
				? "0" + minutes
				: minutes
			)
		);

		return `${hours}:${minutes}`;
	}
);

const makeGetMetricData = () => createStructuredSelector({
	todaysScore: getTodaysScore,
	viewingDate: getViewingDate,
	todaysScoreFormatted: getTodaysScoreFormatted,
});

export const makeMapStateToProps = () => (
	(state, ownProps) => R.merge(
		ownProps,
		makeGetMetricData()(state, ownProps)
	)
);

const AlignRight = styled.div`
	float: right;
`;

const timeStringToSeconds = (value) => {
	const digits = R.pipe(
		R.map(
			(c) => {
				return parseInt(c, 10)
			}
		),

		R.filter(
			(x) => {
				return !Number.isNaN(x);
			}
		),

		R.prepend(0),
		R.prepend(0),
		R.prepend(0),
		R.prepend(0),

		R.reverse,

		R.slice(0, 4),

		R.reverse,

	)(value.split(""))

	const minutes = (digits[2] * 10) + digits[3];
	const hours = (digits[0] * 10) + digits[1];

	return 60 * ( minutes + ( 60 * hours ) );
};


export default connect( makeMapStateToProps )( (props) => (
	<div>
		<AlignRight>

			<RaisedButton
				label = "-"
				disabled = { (props.viewingDate !== today() ) }
				onClick = { () => store.dispatch({
					type: Consts.Actions.User.Metric.SET,
					id: props.id,
					value: props.todaysScore - 60,
				})}
			/>

			<TextField
				id = { "input" + props.id }
				disabled = { (props.viewingDate !== today() ) }
				value = { props.todaysScoreFormatted }
				style = {{
					width: "60px",
				}}

				onChange = { (_, value) => {
					store.dispatch({
						type: Consts.Actions.User.Metric.SET,
						id: props.id,
						value: timeStringToSeconds(value),
					})
				}}

			/>

			<RaisedButton
				label = "+"
				disabled = { (props.viewingDate !== today() ) }
				onClick = { () => store.dispatch({
					type: Consts.Actions.User.Metric.SET,
					id: props.id,
					value: props.todaysScore + 60,
				})}
			/>

		</AlignRight>
	</div>
));

