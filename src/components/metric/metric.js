import React from "react";
import styled from "styled-components";
import { connect, } from "react-redux";
import R from "ramda";

import { createSelector, createStructuredSelector, } from 'reselect'

import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

import Consts from "../../consts";
import store from "../../redux/store";
import today from "../../lib/today";
import { getAverageOfMetric, } from "../../lib/calculator";

import TimeMetric from "./time";
import NumberMetric from "./number";
import ScoreBars from "../multi/scoreBars";

const getMetricName = (store, props) => store.metricNames[props.id];
const getMetricType = (store, props) => store.metricTypes[props.id];
const getMetricScores = (store, props) => store.metricScores[props.id];
const getViewingDate = (store, props) => store.currentlyViewingDate;
const getMetricCreatedOn = (store, props) => store.metricCreatedOn[props.id];

const getTodaysScore = createSelector(
	[
		getMetricScores,
		getViewingDate,
	],
	(
		scores,
		date,
	) => (scores[date])
);

const getAverageScore = createSelector(
	[
		getMetricScores,
		getViewingDate,
		getMetricCreatedOn,
	], getAverageOfMetric
);

const makeGetMetricData = () => createStructuredSelector({
	name: getMetricName,
	type: getMetricType,
	todaysScore: getTodaysScore,
	averageScore: getAverageScore,
});

export const makeMapStateToProps = () => (
	(state, ownProps) => R.merge(
		ownProps,
		makeGetMetricData()(state, ownProps)
	)
);

const PaddedPaper = styled(Paper)`
	margin: 8px;
	padding: 8px;
`;

export default connect( makeMapStateToProps )( (props) => (
	<PaddedPaper>

		<TextField
			value = { props.name }
			hintText = "Task name"
			onChange = { (_, name) => store.dispatch({
				type: Consts.Actions.User.Metric.RENAME,
				id: props.id,
				name,
			})}
		/>

		{
			{
				[Consts.Types.Metric.TIME]: (
					<TimeMetric
						{ ...props }
					/>
				),

				[Consts.Types.Metric.NUMBER]: (
					<NumberMetric
						{ ...props }
					/>
				),

			}[props.type]
		}

		<ScoreBars
			score = { Math.min( props.todaysScore / props.averageScore, 5) }
		/>

	</PaddedPaper>
));
