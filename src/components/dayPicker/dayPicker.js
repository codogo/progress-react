import React from "react";
import { connect, } from "react-redux";
import styled from "styled-components";
import moment from "moment";

import {
	BottomNavigation,
	BottomNavigationItem
} from 'material-ui/BottomNavigation';

import Paper from 'material-ui/Paper';

import store from "../../redux/store";
import Consts from "../../consts";

const Bottom = styled.div`
    position: fixed;
    bottom: 0;
	left: 0;
	width: 100%;
`;

const BottomBuffer = styled.div`
	bottom: 0px;
	height:56px;
	width: 100%;
`;

export default connect(
	(state) => ({
		date: state.currentlyViewingDate,
	})
)((props) => (
	<div>
		<Bottom>
			<Paper
				zDepth = { 2 }
			>

			<BottomNavigation
				selectedIndex = { 1 }
			>

				<BottomNavigationItem
					label = { moment.unix(props.date).subtract(1, "days").format("ddd DD/MM") }
					icon = { <div /> }
					onTouchTap = { () => store.dispatch({
						type: Consts.Actions.User.Day.Move.BACK_ONE,
					})}
				/>

				<BottomNavigationItem
					label = { moment.unix(props.date).format("ddd DD/MM") }
					icon = { <div /> }
					onTouchTap = { () => store.dispatch({
						type: Consts.Actions.User.Day.Move.TODAY,
					})}
				/>

				<BottomNavigationItem
					label = { moment.unix(props.date).add(1, "days").format("ddd DD/MM") }
					icon = { <div /> }
					onTouchTap = { () => store.dispatch({
						type: Consts.Actions.User.Day.Move.FORWARD_ONE,
					})}
				/>

			</BottomNavigation>
		</Paper>

	</Bottom>

	<BottomBuffer />

	</div>
));
