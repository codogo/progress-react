import React from 'react';
import { connect, } from "react-redux";
import styled from "styled-components";
import R from "ramda";

import AppBar from 'material-ui/AppBar';
import LinearProgress from 'material-ui/LinearProgress';

import {
	createSelector,
	createStructuredSelector,
} from 'reselect'

import today from "../../lib/today";

import {
	getTodaysProgress as getTodaysProgress_,
} from "../../lib/calculator";

import DailyScoreBars from "../multi/scoreBars";

const getMetricScores = (store) => store.metricScores;
const getViewingDate = (store) => store.currentlyViewingDate;

const getTodaysProgress = createSelector(
	[
		getMetricScores,
		getViewingDate,
	], getTodaysProgress_,
);

const getTopbarData = createStructuredSelector({
	todaysProgress: getTodaysProgress,
});

const TopWrapper = styled.div` `; 

export default connect( 
	(store, ownProps) => R.merge(
		ownProps,
		getTopbarData(store, ownProps)
	)
)(

	(props) => (
		<TopWrapper>

			<AppBar
				title="Title"
				iconClassNameRight="muidocs-icon-navigation-expand-more"
			/>

		<DailyScoreBars
			score = { props.todaysProgress }
		/>

</TopWrapper>
	)
);
