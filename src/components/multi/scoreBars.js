import React from 'react';
import AppBar from 'material-ui/AppBar';
import LinearProgress from 'material-ui/LinearProgress';

import styled from "styled-components";

export default (props) => (
	<div>
		{
			Array.from({
				length: ( props.score ),
			}).map( (x, i) => (
				<LinearProgress

					key = { i }
					mode="determinate"
					value = { 100 }
					color = "red"
				/>
			))
		}

		<LinearProgress
			mode="determinate"
			value = { ( props.score * 100 ) % 100 }
		/>
	</div>
);
