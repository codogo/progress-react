import React from "react";
import styled from "styled-components";
import { connect, } from "react-redux";
import R from "ramda";

import FloatingActionButton from "material-ui/FloatingActionButton";
import ContentAdd from "material-ui/svg-icons/content/add";
import Popover from "material-ui/Popover";
import Menu from "material-ui/Menu";
import MenuItem from "material-ui/MenuItem";

import Metric from "../metric/metric";

import store from "../../redux/store";
import Consts from "../../consts";
import idGen from "../../lib/idGen";
import today from "../../lib/today";

const AddMetric = styled(FloatingActionButton)`
	position: fixed;
	right: 20px;
	bottom: 76px;
`;

const MetricsWrapper = styled.div`
	flex: 1;

	display: flex;
	flex-direction: row;
	flex-wrap: wrap;

	align-items: flex-start;
	justify-content: space-around;
`;

export default connect(

	(store, ownProps) => R.merge(
		ownProps,
		{
			metricIDs: R.pipe(
				R.identity,
				R.toPairs,

				R.map(
					([ metricID , dailyScores, ]) => ({
						metricID,
						scoreToday: dailyScores[today()]
					})
				),

				R.map( R.prop("metricID") ),

			)(store.metricScores),
		}
	)

)(class MetricsPane extends React.Component {
	constructor(props){
		super(props);

		this.adderClicked = this.adderClicked.bind(this);
		this.closeAdderPopover = this.closeAdderPopover.bind(this);

		this.state = {
			adderOpen: false,
		};
	}

	adderClicked(e){
		e.preventDefault();

		this.setState({
			adderOpen: true,
			adderAnchorEl: e.currentTarget,
		});
	};

	closeAdderPopover(e){
		this.setState({
			adderOpen: false,
		})
	}

	render() {
		return (
			<MetricsWrapper>

				{
					this.props.metricIDs.map( (id) => (
						<Metric
							key = { id }
							id = { id }
						/>
					))
				}

				<AddMetric
					onTouchTap = { this.adderClicked }
				>
					<ContentAdd />
				</AddMetric>

				<Popover
					open = { this.state.adderOpen }
					anchorEl = { this.state.adderAnchorEl }
					anchorOrigin = {{
						vertical: "bottom",
						horizontal: "right",
					}}
					targetOrigin = {{
						vertical: "bottom",
						horizontal: "right",
					}}
					onRequestClose = { this.closeAdderPopover }
				>

				<Menu>
					<MenuItem
						primaryText = "Time Spent"
						onTouchTap = { () => {
							store.dispatch({
								type: Consts.Actions.User.Metric.CREATE_NEW,
								metricType: Consts.Types.Metric.TIME,
								id: idGen(),
							});

							this.closeAdderPopover();
						}}
					/>

				<MenuItem
					primaryText = "Number"
					onTouchTap = { () => {
						store.dispatch({
							type: Consts.Actions.User.Metric.CREATE_NEW,
							metricType: Consts.Types.Metric.NUMBER,
							id: idGen(),
						});

						this.closeAdderPopover();
					}}
				/>
			</Menu>

		</Popover>

	</MetricsWrapper>
		);
	}
});
