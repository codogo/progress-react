import Consts from "../../consts";
import R from "ramda";

const noop = (state) => (state);

export default (state = [], action) => (
	({
		[Consts.Actions.User.Metric.CREATE_NEW]: (state, action) => (
			R.assoc(
				action.id,
				"",
				state
			)
		),

		[Consts.Actions.User.Metric.RENAME]: (state, action) => (
			R.assoc(
				action.id,
				action.name,
				state
			)
		),

	}[action.type] || noop)(state, action)
);


