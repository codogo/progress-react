import Consts from "../../consts";
import R from "ramda";

const noop = (state) => (state);

export default (state = [], action) => (
	({
		[Consts.Actions.User.Metric.CREATE_NEW]: (state, action) => (
			R.assoc(
				action.id,
				action.metricType,
				state
			)
		),

	}[action.type] || noop)(state, action)
);


