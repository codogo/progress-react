import Consts from "../../consts";
import R from "ramda";
import today from "../../lib/today";

const noop = (state) => (state);

const initialState = {}

export default (state = initialState, action) => (
	({
		[Consts.Actions.User.Metric.CREATE_NEW]: (state, action) => (
			(action.metricType === Consts.Types.Metric.TIME)
			? R.assoc(
				action.id,
				{
					[today()]: 0
				},
				state
			)
			: R.assoc(
				action.id,
				{
					[today()]: 0,
				},
				state
			)
		),

		[Consts.Actions.User.Metric.SET]: (state, action) => (
			R.assocPath(
				[ action.id, today(), ],
				(
					(action.value >= 0)
					? action.value
					: 0
				),
				state
			)
		),

	}[action.type] || noop)(state, action)
);


