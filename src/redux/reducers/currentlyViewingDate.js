import moment from "moment";

import Consts from "../../consts";
import today from "../../lib/today";

const initialState = today();
const noop = (state) => (state);

export default ( state = initialState, action) => (
	({
		[Consts.Actions.User.Day.Move.BACK_ONE]: (state) => (
			moment.unix(state).subtract(1, "day").unix()
		),

		[Consts.Actions.User.Day.Move.TODAY]: (state) => (
			today()
		),

		[Consts.Actions.User.Day.Move.FORWARD_ONE]: (state) => (
			moment.unix(state).add(1, "day").unix()
		),

	}[action.type] || noop)(state, action)
);
