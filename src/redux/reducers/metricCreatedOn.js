import Consts from "../../consts";
import R from "ramda";
import today from "../../lib/today";

const noop = (state) => (state);

export default (state = [], action) => (
	({
		[Consts.Actions.User.Metric.CREATE_NEW]: (state, action) => (
			R.assoc(
				action.id,
				today(),
				state
			)
		),

	}[action.type] || noop)(state, action)
);



