import {
	createStore,
	combineReducers,
	compose,
	applyMiddleware,
} from "redux";

import createLogger from "redux-logger";
import LF from "localforage";

import * as reducers from "./reducers/reducers";
import {persistStore, autoRehydrate} from 'redux-persist'

const reducer = combineReducers({
	...reducers,
});

const logger = createLogger({
	collapsed: true,
	duration: true,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
	reducer,
	composeEnhancers(
		applyMiddleware(logger)
	),
	autoRehydrate()
);

persistStore(
	store,
	{
		storage: LF,
	}
);
//).purge();

export default store;
