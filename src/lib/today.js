import moment from "moment";

export default () => moment().startOf("day").unix();
