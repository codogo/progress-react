import R from "ramda";

const atoi = R.memoize(
	(s) => (parseInt(s, 10))
);

export const getAverageOfMetric = (scoresObject, beforeDate) => R.pipe(
	R.toPairs,

	R.map( ([key, val]) => ([
		atoi(key),
		val,
	])),

	R.filter( ([key, val]) => (key < atoi(beforeDate)) ),

	R.sortBy(R.prop(0)),

	R.reverse,

	R.slice(0, 30),

	R.pluck(1),

	R.mean,

)(scoresObject);

export const getTodaysProgress = (allScoresObjects, todayDate) => R.pipe(
	R.toPairs,

	R.map( ([key, scoresObject]) => (
		scoresObject[todayDate] / getAverageOfMetric(scoresObject, todayDate)
	)),

	R.mean,

)(allScoresObjects);
