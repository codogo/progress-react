import crypto from "crypto";

export default () => (
	crypto.randomBytes( Math.ceil( 8 * 3 / 4 ) )
		.toString( "base64" )   // convert to base64 format
		.slice( 0, 32 )        // return required number of characters
		.replace( /(\+|\/)/g, "K" )
);  // replace '+' and '/' with '_'
