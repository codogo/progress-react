import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { InjectGlobals, } from "styled-components";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { Provider, } from "react-redux";

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

import Shell from "./components/shell";
import Topbar from "./components/topbar/topbar";
import Metrics from "./components/metrics/metrics";
import DayPicker from "./components/dayPicker/dayPicker";

import store from "./redux/store";

const muiTheme = getMuiTheme({
	fontFamily: "monospace",
});

const App = (props) => (
	<div 
		className = "shell"
	>
		<Provider
			store = { store }
		>
			<MuiThemeProvider
				muiTheme = { muiTheme }
			>

				<Shell>

					<Topbar />

					<Metrics />

					<DayPicker />

				</Shell>

			</MuiThemeProvider>
		</Provider>
	</div>
);

export default App;
