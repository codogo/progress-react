const Constants = {
	Actions: {
		User: {
			Day: {
				Move: {
					BACK_ONE: "",
					FORWARD_ONE: "",
					TODAY: "",
				},
			},

			Metric: {
				CREATE_NEW: "",
				REMOVE: "",
				RENAME: "",
				SET: "",
			},
		},
	},

	Types: {
		Metric: {
			TIME: "",
			NUMBER: "",
		},
	},
};

const recursiveSpecification = (o, s) => {
	for(var key in o){
		const sWithKey = s + "_" + key.toUpperCase();
		if(key === "Magic" || key === "Themes"){
			continue;
		}
		if(typeof o[key] === "object"){
			recursiveSpecification(o[key], sWithKey);
		}
		else if(typeof o[key] === "string"){
			o[key] = sWithKey;
		}
		else if(typeof o[key] === "number"){
			o[key] = o[key];
		}
	}
}

recursiveSpecification(Constants, "CONST");

export default Constants;
